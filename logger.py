import logging
import os
from itertools import islice
import sys


class CSVLogger():  # pragma: no cover
    def __init__(self, name, log_file, level='info'):
        # create logger on the current module and set its level
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.INFO)
        self.logger.setLevel(getattr(logging, level.upper()))
        self.needs_header = True
        # create a formatter that creates a single line of json with a comma at the end
        self.formatter = logging.Formatter(
            (
                '"%(user_id)s","%(username)s","%(fullname)s","%(bio)s","%(verified)s","%(private)s","%(pic)s","%(followers)s","%(following)s","%(posts)s"'
            )
        )
        self.log_file = log_file
        if self.log_file:
            # create a channel for handling the logger (stderr) and set its format
            ch = logging.FileHandler(log_file)
        else:
            # create a channel for handling the logger (stderr) and set its format
            ch = logging.StreamHandler()
        ch.setFormatter(self.formatter)
        # connect the logger to the channel
        self.logger.addHandler(ch)

    def log(self, user_id, username, fn, bio, verified, private, pic, fl, ff, posts, level='info'):
        HEADER = 'ID,USERNAME,FULLNAME,BIO,VERIFIED,PRIVATE,PROFILE PIC URL, FOLLOWERS, FOLLOWING, POSTS\n'
        if self.needs_header:
            if self.log_file and os.path.isfile(self.log_file):
                with open(self.log_file) as file_obj:
                    if len(list(islice(file_obj, 2))) > 0:
                        self.needs_header = False
                if self.needs_header:
                    with open(self.log_file, 'a') as file_obj:
                        file_obj.write(HEADER)
            else:
                if self.needs_header:
                    sys.stderr.write(HEADER)
            self.needs_header = False
        extra = {
            'user_id':user_id,
            'username':username,
            'fullname':fn,
            'bio':bio,
            'verified':verified,
            'private':private,
            'pic':pic,
            'followers':fl,
            'following':ff,
            'posts':posts
        }
        func = getattr(self.logger, level)
        func("hello", extra=extra)


# if __name__ == "__main__":
#     logger = CSVLogger("object")
#     logger.log("1214","lovelygrace")
        