import requests
import json
import csv
import time, datetime
from bs4 import BeautifulSoup

# 20 users = 18.407070446 seconds [5 tries]
# 50 users = 47.4621180534 seconds [5 tries]
# 100 users = 91.4396423101 seconds [2 tries]

start_time = time.time()
def get_profile():
    users = ["arianagrande", 
            "justinbieber",
            "lalalalisa_m",
            "jennierubyjane",
            "katyperry",
            "juliabarretto", 
            "gracelovely12",
            "bernardokath",
            "supremo_dp",
            "nickiminaj",
            "annehathaway",
            "bran.do.bran.dont",
            "redvelvet.smtown",
            "twicetagram",
            "loonatheworld",
            "weareone.exo",
            "anakarylle",
            "allyviated",
            "stephencurry30",
            "vhongx44"
            ]
    # create and open csv file
    f = csv.writer(open('ig_leads-' + str(datetime.datetime.now()) + '.csv', 'w'))
    f.writerow(['ID', 'USERNAME', 'FULL NAME', 'BIOGRAPHY', 'VERIFIED', 'PRIVATE', 'PROFILE PIC URL', 'FOLLOWERS', 'FOLLOWING', 'POSTS'])

    for u in users:
        url = 'https://www.instagram.com/' + u + '/'
        page = requests.get(url)
        soup = BeautifulSoup(page.text, 'lxml')

        general_profile = soup.find('body')
        main_profile = general_profile.find_all('script')
        user_json = main_profile[0].text
        length = len(user_json)
        # parse string
        user_final = user_json[21:length - 1]
        # transform to json
        user_j = json.loads(user_final)
        user_profile = user_j["entry_data"]["ProfilePage"][0]["graphql"]["user"]

        # get user info
        user_id = user_profile['id']
        user_un = user_profile['username']
        user_fn = user_profile['full_name']
        user_bio = user_profile['biography']
        user_is_verified = user_profile['is_verified']
        user_is_private = user_profile['is_private']
        user_pp = user_profile['profile_pic_url']
        user_followers = user_profile['edge_followed_by']['count']
        user_following = user_profile['edge_follow']['count']
        user_posts = user_profile['edge_owner_to_timeline_media']['count']

        # write to csv file
        f.writerow([user_id, user_un, user_fn, user_bio, user_is_verified, user_is_private, user_pp, user_followers, user_following, user_posts])
    print("Finished: %s seconds" % (time.time() - start_time))
    

if __name__ == "__main__":
  get_profile()